
import com.mongodb.client.MongoCollection;
import lombok.Getter;
import lombok.Setter;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bukkit.entity.Player;
import [redacted].Ban;

import java.util.UUID;

public class BanData {

    private MongoCollection bans = MongoManager.getDatabase().getCollection("bans");


    @Getter
    @Setter
    public static BanData instance;



    public boolean playerExists(UUID uuid) {
        Document doc = (Document) bans.find(new Document("uuid", uuid.toString())).first();
        return doc != null;
    }

    public void createPlayer(Player player) {
        if (playerExists(player.getUniqueId())) { // update the name if the player changed it
            Document doc = (Document) bans.find(new Document("uuid", player.getUniqueId().toString())).first();
            if (!doc.getString("name").equals(player.getName())) {
                Bson updatedVal = new Document("name", player.getName());
                Bson updatedOp = new Document("$set", updatedVal);
                bans.updateOne(doc, updatedOp);
            }
            // check for name change
            return;
        }
        Document doc = new Document("uuid", player.getUniqueId().toString());
            doc.append("end-time", 0).
            append("end-time", "0").
            append("id", "")
            .append("reason", "")
            .append("name", player.getName());

        // noinspection all
        bans.insertOne(doc);
    }

    public boolean isBanned(UUID uuid) {
        if (!playerExists(uuid)) {
            return false;
        }
        Document doc = (Document) bans.find(new Document("uuid", uuid.toString())).first();
        if (doc.getString("end-time").equals("-1")) {
            return true;
        }
        if (System.currentTimeMillis() < Long.parseLong(doc.getString("end-time"))) {
            return true;
        }
        return false;
    }

    public Ban getBan(UUID uuid) {
        Document doc = (Document) bans.find(new Document("uuid", uuid.toString())).first();
        return new Ban(uuid, Ban.BanReason.fromID(doc.getString("reason")), Long.parseLong(doc.getString("end-time")), doc.getString("reason"), doc.getString("id"));
    }

    public Ban getBan(String ID) { // get it from the ban id
        Document doc = (Document) bans.find(new Document("id", ID)).first();
        return new Ban(UUID.fromString(doc.getString("uuid")), Ban.BanReason.fromID(doc.getString("reason")),
                Long.parseLong(doc.getString("end-time")), doc.getString("reason"), doc.getString("id"));
    }

    public void banPlayer(Ban ban) { //TODO
        UUID uuid = ban.getUuid();
        Document doc = (Document) bans.find(new Document("uuid", uuid.toString()));
        boolean permanent = ban.getEndTime() == -1;
        if (permanent){

        } else {

        }
    }



}
